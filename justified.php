<?php

fscanf(STDIN, '%d', $L);
$T = readline();

$arrayString = explode(" ", $T);
$printString = "";

foreach ($arrayString as $key => $value) {

	$stringTemp = trim($printString." ".$value);

	if (strlen($stringTemp) <= $L) {
		$printString = $stringTemp;
	} else {
		echo $printString."\n";
		$printString = $value;
	}
}

echo $printString."\n";